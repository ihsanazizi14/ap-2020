package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    HolyGrail holyGrail;
    @BeforeEach
    public void setUp() throws Exception {

        holyGrail = new HolyGrail();
        holyGrail.makeAWish("Class Passed!");
    }

    @Test
    public void testSetWish() {
        holyGrail.makeAWish("Test Passed!");
        assertEquals("Test Passed!", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void testGetWish() {
        assertEquals("Class Passed!", holyGrail.getHolyWish().getWish());
    }


}
