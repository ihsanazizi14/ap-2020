package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");

    }

    @Test
    public void checkKnightInstances() {
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());

    }

    @Test
    public void checkKnightDescriptions() {
        assertNull(majesticKnight.getSkill());
        assertTrue(majesticKnight.getArmor() instanceof MetalArmor);
        assertTrue(majesticKnight.getWeapon() instanceof ThousandJacker);

        assertNull(metalClusterKnight.getWeapon());
        assertTrue(metalClusterKnight.getArmor() instanceof MetalArmor);
        assertTrue(metalClusterKnight.getSkill() instanceof ThousandYearsOfPain);

        assertNull(syntheticKnight.getArmor());
        assertTrue(syntheticKnight.getWeapon() instanceof ThousandJacker);
        assertTrue(syntheticKnight.getSkill() instanceof ThousandYearsOfPain);

    }

}
