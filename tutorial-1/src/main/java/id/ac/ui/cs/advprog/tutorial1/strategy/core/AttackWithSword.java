package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    public String attack(){
        return "Attack with Pedang !!!";

    }
    public String getType(){
        return "Pedang";
    }
}
